#!/bin/bash

# SHELL OPTIONS
set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable
set -o pipefail

# SELF
DIR="$( dirname "${BASH_SOURCE[0]}" )/../"
REPO_ROOT_ABSPATH="$(realpath ${DIR})"

# IN
DOCUMENTATION_DIR="${REPO_ROOT_ABSPATH}/documentation"
DOCUMENTATION_BUILD_SCRIPT="${DOCUMENTATION_DIR}/build.sh"
DOCUMENTATION_IMAGES_VARS_OUTFILE="${DOCUMENTATION_DIR}/content/variables/images.yaml"
BUILT_HTML="${DOCUMENTATION_DIR}/build/README.html"

NETWORK_DIR="${REPO_ROOT_ABSPATH}/network"
SCRIPTS_DIR="${REPO_ROOT_ABSPATH}/scripts"
IMAGES_ENV_FILE="${REPO_ROOT_ABSPATH}/versions.env"

# network/ related vars
NETWORK_IMAGES_ENV_OUTFILE="${NETWORK_DIR}/.images.env"
NETWORK_GENERATION_SCRIPT="${NETWORK_DIR}/build.sh"
BUILT_NETWORK_DIR="${NETWORK_DIR}/generated"
PYTHON_DIR="${SCRIPTS_DIR}/python"
ALIAS_GENERATION_SCRIPT="${PYTHON_DIR}/make_nv_aliases.py"
ARTIFACT_VERSION=$(cat ${REPO_ROOT_ABSPATH}/VERSION.txt)

# OUT
ROOT_OUT_DIR="${REPO_ROOT_ABSPATH}/package"
ROOT_OUT="xandbox"
OUT="${ROOT_OUT_DIR}/${ROOT_OUT}"

# TEMP
TMP="${REPO_ROOT_ABSPATH}/tmp"

# HELP PRINTOUT
print_help() {
  HELPTEXT="$(cat << END
    This script packages xandbox by running the network generation script, the documentation build script, and then zipping up the built artifacts.

    Options
      -h: Print help
      -i: Install the expected version of XNG during generation
      -c: (Required) Container registry string. E.g. \"gcr.io/xand-dev\" or \"transparentinc-docker-external.jfrog.io\"
    
    Input Variables:
      ARTIFACTORY_USER=value       Artifactory Username (required)
      ARTIFACTORY_PASS=value       Artifactory Password (required)
END
  )"
  echo -e "$HELPTEXT"
}

# Args parsing pulled from last post here: https://www.unix.com/shell-programming-and-scripting/59607-getopts-alternative.html
while true
do
  case $# in 0) break ;; esac
  case $1 in
    -i) INSTALL_XNG=1; shift ;;
    -c) shift; CONTAINER_REGISTRY=$1; shift ;;
    -h|--help) print_help && exit 0 ;;
    -*) echo "Invalid option $1" && print_help && exit 1 ;;
    *) break ;;
  esac
done

# Verify required input variables are set
[[ -z "${CONTAINER_REGISTRY:-""}" ]] && echo "ERROR: Mising container registry string." && print_help && exit 1;
[[ -z "${ARTIFACTORY_USER:-""}" ]] && echo "ERROR: ARTIFACTORY_USER must be set." && print_help && exit 1;
[[ -z "${ARTIFACTORY_PASS:-""}" ]] && echo "ERROR: ARTIFACTORY_PASS must be set." && print_help && exit 1;

# Trim trailing '/' if exists
CONTAINER_REGISTRY="${CONTAINER_REGISTRY%/}"
echo "Using container registry: ${CONTAINER_REGISTRY}"

clean_build_dir() {
    rm -rf ${ROOT_OUT_DIR}
    mkdir -p ${OUT}
}

load_images_env_file() {
  source ${IMAGES_ENV_FILE}
}

generate_xandbox_network() {
    echo -e "(Re)generating network"
    echo ${CONTAINER_REGISTRY}
    sed "s|\${CONTAINER_REGISTRY}|${CONTAINER_REGISTRY}|g" ${IMAGES_ENV_FILE} > ${NETWORK_IMAGES_ENV_OUTFILE}

    if [ ${INSTALL_XNG:-0} -eq 1 ]; 
    then 
      ${NETWORK_GENERATION_SCRIPT} -i --images-env-filepath ${NETWORK_IMAGES_ENV_OUTFILE}; 
    else 
      ${NETWORK_GENERATION_SCRIPT} --images-env-filepath ${NETWORK_IMAGES_ENV_OUTFILE}; 
    fi
    cp -R ${BUILT_NETWORK_DIR} ${OUT}/network
}

generate_xandbox_documentation() {
    echo -e "(Re)generating documentation"

    # Build variables/images.yaml file for document generation to use
    cat > ${DOCUMENTATION_IMAGES_VARS_OUTFILE} << EOF
IMAGES:
  MEMBER-CONFIGURATION-IMAGE: "${MEMBER_CONFIGURATION_IMAGE}"
EOF

    ${DOCUMENTATION_BUILD_SCRIPT}
    cp ${BUILT_HTML} ${OUT}
}

generate_nv_aliases() {
    echo -e "(Re)generating Network Voting aliases"
    echo -e "\tUsing: ${NV_CLI_IMAGE}"
    python3 -m pip install -r ${PYTHON_DIR}/requirements.txt
    python3 ${ALIAS_GENERATION_SCRIPT} ${OUT}/network/entities-metadata.yaml ${OUT}/nv_aliases.sh --nv-cli-image ${NV_CLI_IMAGE}
}

copy_xandbox_version_txt_file() {
  cp ${REPO_ROOT_ABSPATH}/VERSION.txt "${OUT}"
}

package_zip() {
    echo -e "Zipping up Xandbox"
    (cd ${OUT} && zip -qq -r ../${ROOT_OUT}_${ARTIFACT_VERSION}.zip .)
}

load_images_env_file
clean_build_dir
generate_xandbox_network
generate_xandbox_documentation
generate_nv_aliases
copy_xandbox_version_txt_file
package_zip
