import argparse
import docker
from docker.models.images import Image
from dotenv import dotenv_values
from typing import Dict


def _get_args() -> Dict[str, str]:
    """Parses the arguments passed to the python script. Argparse will also display help messages to help the user
    pass the right arguments.
    """

    cli_description = (
        """Given a set of images, will pull, re-tag and push them to target registry"""
    )

    parser = argparse.ArgumentParser(description=cli_description)

    parser.add_argument(
        "--images-env-file",
        type=str,
        help="Env file containing set of fully qualified docker images",
        metavar=".images_env_file",
        required=True,
    )

    parser.add_argument(
        "--source-registry",
        type=str,
        help="Source registry to pull images from",
        metavar=".source_registry",
        required=True,
    )

    parser.add_argument(
        "--target-registry",
        type=str,
        help="Target registry to release images to",
        metavar=".target_registry",
        required=True,
    )

    args = parser.parse_args()
    if not args.images_env_file or not args.target_registry or not args.source_registry:
        parser.print_usage()
        exit(1)
    return args


class DockerImage:
    def __init__(self, registry, name, tag):
        self.registry = registry
        self.name = name
        self.tag = tag

    def fully_qualified_name(self) -> str:
        return f"{self.repository()}:{self.tag}"

    def repository(self) -> str:
        """Returns <registry>:<name>"""
        return f"{self.registry}/{self.name}"

    @staticmethod
    def from_str(input) -> "DockerImage":
        [head, tag] = input.split(":", 1)
        [registry, name] = head.rsplit("/", 1)
        return DockerImage(registry, name, tag)


if __name__ == "__main__":
    args = _get_args()
    images = dotenv_values(args.images_env_file)
    source_registry = args.source_registry
    target_registry = args.target_registry
    print(f"Loaded: {images}")

    docker_client = docker.from_env()

    # Pull all images, and tag with target registry, saving into new array
    target_images = []
    for (k, v) in images.items():
        img = DockerImage.from_str(v)
        img.registry = source_registry
        print(f"Pulling {img.fully_qualified_name()}")
        # Pull
        pulled_img: Image = docker_client.images.pull(img.repository(), tag=img.tag)
        # Tag
        succeeded = pulled_img.tag(f"{target_registry}/{img.name}", tag=img.tag)
        if not succeeded:
            raise Exception(
                f"Failed tagging into repo {target_registry}. Source: {img.fully_qualified_name()}"
            )
        # Save
        target_images.append(DockerImage(target_registry, img.name, img.tag))
    
    # Push to target registry
    for img in target_images:
        print(f"Pushing {img.fully_qualified_name()}")
        for json_msg in docker_client.images.push(img.repository(), tag=img.tag, stream=True, decode=True):
            if "errorDetail" in json_msg:
                raise Exception(json_msg)
            print(json_msg)
