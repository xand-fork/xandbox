#!/bin/env python3

"""
Given the metadata for a generated network, produces a Bash "source"able aliases file which makes
available easy commands to act on behalf of a chosen Member or Validator in the Network Voting CLI.

Prerequisites:
    pip3 install -r requirements.txt
"""

import yaml
import argparse
from typing import Dict


class NodeInfo:
    def __init__(self, name, address, url, jwt):
        self.name = name
        self.address = address
        self.url = url
        self.jwt = jwt

    @staticmethod
    def load_from_node_metadata(metadata_obj) -> "NodeInfo":
        return NodeInfo(
            name=metadata_obj["entity-name"],
            address=metadata_obj["address"],
            url=metadata_obj["xand-api-details"]["xand-api-url"],
            jwt=metadata_obj["xand-api-details"]["auth"]["token"]
            if metadata_obj["xand-api-details"]["auth"]
            else None,
        )

    def generate_alias(self, nv_cli_image: str) -> str:
        command = self.generate_entry_command(nv_cli_image)
        escaped_command = command.replace("'", "\\'").replace('"', '\\"')
        snakecase_entity_name = self.name.replace("-", "_")
        return f"alias act_as_{snakecase_entity_name}=$'{escaped_command}'"

    def generate_entry_command(self, nv_cli_image: str) -> str:
        # Example:
        # docker run -it --rm --entrypoint /bin/bash --net=host {nv_cli_image} -c 'bash --init-file <(echo ". \"$HOME/.bashrc\"; export PS1=\"member-0$ \"; network-voting-cli set-config --issuer 5DesW2g9miJMahngjaEVHap6kBMiDH33CdgndL5d75GkY7oz --url http://localhost:10044/ > /dev/null")'
        jwt_opts = f"-j {self.jwt}" if self.jwt else ""
        nv_config_command = f"network-voting-cli set-config --issuer {self.address} --url {self.url} {jwt_opts} > /dev/null"
        return f'docker run -it --rm --entrypoint /bin/bash --net=host {nv_cli_image} -c \'bash --init-file <(echo ". \\"$HOME/.bashrc\\"; export PS1=\\"{self.name}$ \\"; {nv_config_command}")\''


def _get_args() -> Dict[str, str]:
    """Parses the arguments passed to the python script. Argparse will also display help messages to help the user
    pass the right arguments.
    """

    cli_description = """Given the metadata for a generated network, produces a Bash "source"able aliases file which makes
    available easy commands to act on behalf of a chosen Member or Validator in the Network Voting CLI."""

    parser = argparse.ArgumentParser(description=cli_description)

    parser.add_argument(
        "metadata_file_path",
        type=str,
        help="Filepath to a generated network's entities-metadata.yaml file",
    )

    parser.add_argument(
        "output_file_path",
        type=str,
        help="Output file for resulting source-able bash script",
    )

    parser.add_argument(
        "--nv-cli-image",
        type=str,
        required=True,
        metavar=".nv_cli_image"
    )
    args = parser.parse_args()
    if not args.metadata_file_path or not args.output_file_path:
        parser.print_usage()
        exit(1)

    return args


if __name__ == "__main__":
    args = _get_args()
    metadata_file_path = args.metadata_file_path
    output_file_path = args.output_file_path
    nv_cli_image = args.nv_cli_image # "gcr.io/xand-dev/network-voting-cli:0.13.0"

    with open(metadata_file_path, "r") as file:
        metadata_obj = yaml.safe_load(file)

    member_infos = [
        NodeInfo.load_from_node_metadata(meta) for meta in metadata_obj["members"]
    ]
    validator_infos = [
        NodeInfo.load_from_node_metadata(meta) for meta in metadata_obj["validators"]
    ]

    member_aliases = [info.generate_alias(nv_cli_image) for info in member_infos]
    validator_aliases = [info.generate_alias(nv_cli_image) for info in validator_infos]

    with open(output_file_path, "w") as outfile:
        outfile.write("# Members\n")
        outfile.writelines((f"{alias}\n" for alias in member_aliases))
        outfile.write("# Validators\n")
        outfile.writelines((f"{alias}\n" for alias in validator_aliases))
