# Xandbox

## Overview

This project contains the logic to create and package a `xandbox.zip` locally and publish via CI.

You can see the Xandbox documentation rendered in GitLab pages here: https://transparentincdevelopment.gitlab.io/product/apps/xandbox/

## License

This project is licensed under MIT OR Apache-2.0

## Components

### Documentation

`./documentation` contains build logic and content for Xandbox documentation.

#### Content

Content for public consumption live in `./documentation/content/`. 
Written markdown must be [github-flavored](https://github.github.com/gfm/) for best compatibility with `pandoc`.

The goals of documentation are to:

1. Maintain a set of walkthrough scenarios that teaches users how to interact with Xandbox
1. Maintain a thin set of reference documentation

#### Compiling Documentation

Run:
```bash
./documentation/build.sh
```
and visit the `./documentation/build` directory to see the compiled documentation.

The `./documentation/build.sh` script
 
- collects files within the `./documentation/content` directory into 1 common markdown file
- replaces template variables in the 1 markdown file
- converts the markdown file to desired output formats (HTML)

It uses [pandoc](https://pandoc.org/MANUAL.html).

##### Variable substitution

By convention, we follow the first variable format found in the [pandoc manual](https://pandoc.org/MANUAL.html#interpolated-variables).

For example, content of
```markdown
Welcome, $username$!
```
with an entry in the `content/variables.yaml` file of 
```yaml
username: FooBar
```
will result in 
```markdown
Welcome, FooBar!
```

### Network

The `./network` dir contains configuration and a script to create a Xandbox network.  
The versions listed in `./network/.*.env` files *should be kept up to date with Xand Acceptance Tests*.

Run 
```bash
./network/build.sh -i
```
to build a `./network/generated/` network XNG output.  You'll need a temporary `.images.env` file generated from the main `scripts/package.sh` script first as a dependency.

## Versioning

The resulting `xandbox_${VERSION}.zip` is versioned using the value in `VERSION.txt`.

## CI / CD Artifacts

### Tag-To-Publish

Push a tag in the form of `v${VERSION}` to release docker images and a stable version of xandbox to:  
- transparentinc-docker-external.jfrog.io
- https://transparentinc.jfrog.io/artifactory/artifacts-internal/xandbox/xandbox_$VERSION.zip

> **NOTE:** Update the onboarding docs below with the latest stable version after releasing

### Beta Artifacts
Master branch automatically publishes beta version to:
- https://transparentinc.jfrog.io/artifactory/artifacts-internal/xandbox/xandbox_$VERSION-beta-$CI_PIPELINE_IID.zip

All Development branches have manual jobs for publishing a beta package.
## Onboarding

See Xandbox onboarding steps here: https://transparentfinancialsystems.sharepoint.com/:f:/s/TransparentSystems/En4DX-odfcxFrz0pr8F9vBsBYYCtWbVusKKNbwbyKqVeXg?e=Ee3pX4
