#!/bin/bash

# SHELL OPTIONS
set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable
set -o pipefail

# SELF
DIR="$( dirname "${BASH_SOURCE[0]}" )"
ABS_PATH_DIR="$(realpath ${DIR})"

# IN
CONFIG_TEMPLATE="${ABS_PATH_DIR}/config.template.yaml"
DEFAULTS_CONFIG="${ABS_PATH_DIR}/.defaults.env"
ARTIFACTS_CONFIG="${ABS_PATH_DIR}/.artifacts.env"

# OUT
OUT_DIR=${ABS_PATH_DIR}
OUT_NAME="generated"
RECEIPT_DIR_NAME="generated-receipt"
OUT=${OUT_DIR}/${OUT_NAME}
RECEIPT_OUT=${OUT}/${RECEIPT_DIR_NAME}
RECEIPT_OUT_CONFIG="${RECEIPT_OUT}/generated.config.yaml"
RECEIPT_OUT_ARTIFACTS="${RECEIPT_OUT}/used-artifacts.txt"

# TEMP
TMP="${ABS_PATH_DIR}/tmp"

# HELP PRINTOUT
print_help() {
  HELPTEXT="$(cat << END
    This script generates a network via XNG using variables listed in .*.env files and the configuration template config.template.yaml.

    Options
      -h: Print help
      -i: Install the expected version of XNG during generation
    
    Input Variables:
      ARTIFACTORY_USER=value       Artifactory Username (required)
      ARTIFACTORY_PASS=value       Artifactory Password (required)
END
  )"
  echo -e "$HELPTEXT"
}

# Args parsing pulled from last post here: https://www.unix.com/shell-programming-and-scripting/59607-getopts-alternative.html
while true
do
  case $# in 0) break ;; esac
  case $1 in
    -i) INSTALL_XNG=1; shift ;;
    --images-env-filepath) shift; IMAGES_CONFIG=$1; shift ;;
    -|--) shift; break;;
    -h|--help) print_help && exit 0 ;;
    -*) echo "Invalid option $1" && print_help && exit 1 ;;
    *) break ;;
  esac
done

# Validate required input args
[[ -z "${IMAGES_CONFIG:-""}" ]] && { echo "Missing filepath for --images-env-filepath" ; exit 1; }

install_xng() {
    if [ ${INSTALL_XNG:-0} -eq 1 ]; then cargo install xand_network_generator --locked --registry tpfs --version ${XAND_NETWORK_GENERATOR_VERSION}; fi
}

clean_build_dir() {
  rm -rf ${OUT}
}

create_temp_dir() {
  mkdir -p ${TMP}
}

# Validate input .env file containing docker image versions
source_and_validate_images_env() {
  source $IMAGES_CONFIG

  [[ -z "${MEMBER_API_IMAGE}" ]] && { echo "Missing MEMBER_API_IMAGE in input env file" ; exit 1; }
  [[ -z "${VALIDATOR_IMAGE}" ]] && { echo "Missing VALIDATOR_IMAGE in input env file" ; exit 1; }
  [[ -z "${TRUST_IMAGE}" ]] && { echo "Missing TRUST_IMAGE in input env file" ; exit 1; }
  [[ -z "${BANK_MOCKS_IMAGE}" ]] && { echo "Missing BANK_MOCKS_IMAGE in input env file" ; exit 1; }

  echo "${IMAGES_CONFIG} validated"
}

load_vars() {
  source ${DEFAULTS_CONFIG}
  source ${ARTIFACTS_CONFIG}
}

get_chainspec() {
  zip_filename="chain-spec-template.${CHAINSPEC_VERSION}.zip"

  wget --directory-prefix=${TMP} --user=${ARTIFACTORY_USER}  --password=${ARTIFACTORY_PASS} -O "${TMP}/chainspec.zip" \
    https://transparentinc.jfrog.io/artifactory/artifacts-internal/chain-specs/templates/${zip_filename}
}

generate_config() {
  cp ${CONFIG_TEMPLATE} ${TMP}/config.yaml

  declare -A replacements

  replacements[MEMBER_API_IMAGE]=${MEMBER_API_IMAGE}
  replacements[VALIDATOR_IMAGE]=${VALIDATOR_IMAGE}
  replacements[TRUST_IMAGE]=${TRUST_IMAGE}
  replacements[BANK_MOCKS_IMAGE]=${BANK_MOCKS_IMAGE}
  replacements[TRUST_API_IMAGE]=${TRUST_API_IMAGE}
  replacements[MINOR_UNITS_PER_VALIDATOR_EMISSION]=${MINOR_UNITS_PER_VALIDATOR_EMISSION}
  replacements[BLOCK_QUOTA]=${BLOCK_QUOTA}

  replacements[CONFIDENTIAL_MODE]=${DEFAULT_CONFIDENTIAL_MODE}
  replacements[VALIDATOR_COUNT]=${DEFAULT_VALIDATOR_COUNT}
  replacements[MEMBER_COUNT]=${DEFAULT_MEMBER_COUNT}
  replacements[ENABLE_AUTH]=${DEFAULT_ENABLE_AUTH}

  echo -e "Creating XNG Config"
  for k in "${!replacements[@]}"
    do
        echo -e "\tReplacing ${k} with ${replacements[${k}]}"
        sed -i -e "s@\${${k}}@${replacements[${k}]}@" ${TMP}/config.yaml
    done
}

generate_xandbox_network() {
  xand_network_generator generate  \
    --config ${TMP}/config.yaml \
    --chainspec-zip ${TMP}/"chainspec.zip" \
    --output-dir ${OUT}
}

generate_receipt() {
  mkdir -p ${RECEIPT_OUT}
  cp ${TMP}/config.yaml ${RECEIPT_OUT_CONFIG}
  echo "XNG Version: ${XAND_NETWORK_GENERATOR_VERSION}" >> ${RECEIPT_OUT_ARTIFACTS}
  echo "Chainspec Version: ${CHAINSPEC_VERSION}" >> ${RECEIPT_OUT_ARTIFACTS}
}

delete_temp_dir() {
    rm -rf ${TMP}
}

source_and_validate_images_env
delete_temp_dir
clean_build_dir
create_temp_dir
load_vars
install_xng
get_chainspec
generate_config
generate_xandbox_network
generate_receipt
delete_temp_dir
