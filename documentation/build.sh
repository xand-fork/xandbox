#!/bin/bash

# SHELL OPTIONS
set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable
set -o pipefail

# SELF
DIR="$( dirname "${BASH_SOURCE[0]}" )"
ABS_PATH_DIR="$(realpath ${DIR})"
ENV_DIR="${ABS_PATH_DIR}/build.env"
TMP="${ABS_PATH_DIR}/tmp" # Volume-mount target for pandoc docker container

PANDOC_LATEX_DOCKER_IMAGE="pandoc/latex:2.14.0.2"

# Variables for building input paths
ASSETS_DIR_NAME="assets"
CONTENT_DIR_NAME="content"
STYLE_DIR_NAME="style"
VARS_DATA_REL_PATH=${CONTENT_DIR_NAME}/variables/data.yaml
VARS_IMAGES_REL_PATH=${CONTENT_DIR_NAME}/variables/images.yaml
STYLE_REL_PATH=${STYLE_DIR_NAME}/style.css
HTML_TEMPLATE_REL_PATH=${STYLE_DIR_NAME}/pandoc-template.html5

# Variables for building output paths and filenames
OUTPUT_DIR_NAME="build"
HTML_OUTPUT="README.html"

# CONSTRUCTED SETTINGS
ASSETS_ABS_PATH="${ABS_PATH_DIR}/${ASSETS_DIR_NAME}"
CONTENT_ABS_PATH_DIR="${ABS_PATH_DIR}/${CONTENT_DIR_NAME}"
STYLE_ABS_PATH="${ABS_PATH_DIR}/${STYLE_DIR_NAME}"

# Get list of markdown files within content/* dir
# `-printf "%P\n"` prints the filename relative to CONTENT_ABS_PATH_DIR
DOCS=$(find "${CONTENT_ABS_PATH_DIR}" -type f -name "*.md" -printf "${CONTENT_DIR_NAME}/%P\n" | sort)

OUTPUT_DIR="${ABS_PATH_DIR}/${OUTPUT_DIR_NAME}"

create_build_dirs() {
  rm -rf ${OUTPUT_DIR}/*
  rm -rf ${TMP}
  mkdir -p ${OUTPUT_DIR}
}

move_temp_files() {
  mkdir -p ${TMP}
  cp -r ${CONTENT_ABS_PATH_DIR} ${TMP}
  cp -r ${STYLE_ABS_PATH} ${TMP}
}

generate_md_template() {
  echo -e "Generating markdown template from docs templates..."
  docker run --rm \
    --volume "${TMP}:/data" \
    --volume ${OUTPUT_DIR}:${OUTPUT_DIR} \
    --volume ${ASSETS_ABS_PATH}:/data/assets \
    ${PANDOC_LATEX_DOCKER_IMAGE} \
      --wrap preserve \
      --to="gfm" \
      --from="gfm" \
      -o ${OUTPUT_DIR}/README.md.template \
      ${DOCS}
  echo -e "Done."
}

generate_md() {
  echo -e "Generating markdown from markdown template..."
  docker run --rm \
    --volume "${TMP}:/data" \
    --volume ${OUTPUT_DIR}:${OUTPUT_DIR} \
    --volume ${ASSETS_ABS_PATH}:/data/assets \
    ${PANDOC_LATEX_DOCKER_IMAGE} \
      --wrap preserve \
      --template=${OUTPUT_DIR}/README.md.template \
      --metadata-file=${VARS_DATA_REL_PATH} \
      --metadata-file=${VARS_IMAGES_REL_PATH} \
      --to="gfm" \
      --from="gfm" \
     -o ${OUTPUT_DIR}/README.md \
     /dev/null
  echo -e "Done."
}

generate_html_from_markdown() {
  echo -e "Generating HTML from markdown..."
  docker run --rm \
    --volume "${TMP}:/data" \
    --volume ${OUTPUT_DIR}:${OUTPUT_DIR} \
    --volume ${ASSETS_ABS_PATH}:/data/assets \
    ${PANDOC_LATEX_DOCKER_IMAGE} \
      --from=gfm \
      --standalone \
      --resource-path ${ASSETS_DIR_NAME} \
      --toc \
      --toc-depth 4 \
      -V block-headings \
      --self-contained \
      --metadata title=Xandbox \
      --section-divs \
      --css=${STYLE_REL_PATH} \
      --template=${HTML_TEMPLATE_REL_PATH} \
      -o "${OUTPUT_DIR}/README.html" \
      ${OUTPUT_DIR}/README.md
  echo -e "Done."
}

clean_temp() {
  rm -rf ${TMP}
}


clean_temp
create_build_dirs
move_temp_files
generate_md_template
generate_md
generate_html_from_markdown
clean_temp

echo -e "Finished building Xandbox Documentation."