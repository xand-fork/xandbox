# Using Xandbox to Move Money

## Introduction

Members interact with the Xand network through their Member API. This Member API is a RESTful Web API service that exposes resources for an individual Member.  Each Member in the Xandbox network will have their own deployed Member API, which will allow them to:

1. Create claims on the Xand network from a bank account
1. Redeem claims from the Xand network to a bank account
1. Send claims between Xand Members
1. View the balance of a Xand Member's wallet

In the following walkthrough, you will be transacting between two sandbox Members:

- Member 0 
- Member 1

First, you will create claims on behalf of Member 0. Then, you will send money from Member 0 to Member 1. Finally, 
you will redeem those claims into Member 1's bank account.

## Create Workflow

The following steps transfer funds from Member 0's bank account to their wallet.

### Verify Starting Balance

1.  Open Member 0's API at [$MEMBER-0-API-BASE-URL$]($MEMBER-0-API-BASE-URL$)

    ![](assets/member-api-ui.png "Member API User Interface")
1.  Click `Authorize`.

    ![](assets/member-api-swagger-auth-button.png "MemberAPI authorize button")
1.  Authenticate with the Member API by pasting the token below into the input field and click `Authorize`.

    `$MEMBER-0-API-TOKEN$`

    ![](assets/member-api-swagger-auth-dialog.png "MemberAPI authorize dialog")
1.  Verify the balance in Member 0's wallet. Select the `/member/balance` section, click `Try it out`, and click `Execute`

    ![](assets/member-api-create-balance.png "Check balance")
1. Scroll down to Responses and verify the balance is zero.

    ![](assets/member-api-create-balance-response-zero.png "Balance result")

A balance of $$0.00 verifies you start with no money in Member 0's wallet.
    
### Initiate Create Request

1.  Select `/transactions/claims/create` and click `Try it out`.

    ![](assets/member-api-create-try.png "Create Request")
1.  Paste the JSON formatted text below into the request body, and click `Execute`.
    ```json
    {
      "accountId": 1,
      "amountInMinorUnit": 50000000
    }
    ``` 

    ![](assets/member-api-create-request-body.png "Create Request body")

1.  Scroll down to Responses and save the `Response Body` JSON text.

    ![](assets/member-api-create-request-save-response.png "Create Request")

    This places a record on the network, initiating a Create Request for Member 0.
    Once the Trustee receives a matching book transfer at the specified bank, they will confirm the funds were received and a claim will be issued to Member 0's wallet.

### Transfer Funds

Fund the Create with:

1.  Click `/transactions/claims/{transactionId}/fund` and click `Try it out`.

    ![](assets/member-api-create-reserve.png "Transfer Funds")
1.  Paste the JSON formatted text below into the request body. Replace the CorrelationId value with the CorrelationId from the Create response saved in an earlier step.  Enter the transactionId saved from the same response into the `transactionId` parameter. Click
    `Execute`.

    ```json
    {
      "amountInMinorUnit": 50000000,
      "correlationId": "your-correlation-id-goes-here",
      "bankAccountId": 1
    }
    ```

    ![](assets/member-api-create-reserve-body.png "Transfer Funds Request body")

    This initiates a book transfer from the Member's bank account to the Trustee's bank account. When the Trustee software finds a matching book transfer and Create Request, it will confirm the receipt of funds and a claim in that amount will be created for the requesting Member.

1.  Select `/transactions/{transactionId}` and click `Try it out`.

    ![](assets/member-api-create-status.png "Create request status")
1.  Paste the transaction ID, saved in the earlier step, into the `transactionId` and click `Execute`.
    
    ![](assets/member-api-create-status-execute.png "Create request execute status")
1.  Scroll down to Responses.

    ![](assets/member-api-create-status-confirmed.png "Transaction status")

    This shows the status of the create request transaction. The Create Request starts with a `pending` state, and 
    changes to `confirmed` when it is complete. Click `execute` again until the `state` is `confirmed`. 

### Verify Funds Transfer

1.  Check your balance again.

    ![](assets/member-api-create-balance-response.png "Balance result")

    A balance of $$500,000.00 shows that the Create Workflow is complete, and that the funds have transferred from the bank
    account to the wallet.

You have now completed the Create workflow!

## Send Workflow

Now that Member 0 has claims on the network, they can send them to Member 1.
You will act as Member 0 in the actions below and observe the results from Member 1's perspective.

### Verify Starting Balances

1.  Open Member 1's API at [$MEMBER-1-API-BASE-URL$]($MEMBER-1-API-BASE-URL$) and authorize yourself with the following token:

    `$MEMBER-1-API-TOKEN$`

    ![](assets/member-api-ui.png "Member API User Interface")
1.  Verify Member 1 has zero balance in their wallet.

    ![](assets/member-api-send-zero-balance.png "Balance result")
1.  Open Member 0's API at [$MEMBER-0-API-BASE-URL$]($MEMBER-0-API-BASE-URL$) and authorize yourself with the following token:

    `$MEMBER-0-API-TOKEN$`

    ![](assets/member-api-ui.png "Member API User Interface")
1.  Verify the balance.

    ![](assets/member-api-create-balance-response.png "Balance result")

The wallet must have a balance greater than $$0 in order to continue. Please
complete the Create Workflow above if there is no balance in Member 0's wallet before continuing.

### Send

1.  Select `/transactions/claims/send` and click `Try it out`.

    ![](assets/member-api-send-payments.png "Send")
1.  Paste the JSON formatted text below into the request body and click `Execute`.

    ```json
    {
      "toAddress": "$MEMBER-1-ADDRESS$",
      "amountInMinorUnit": 100000
    }
    ```
    ![](assets/member-api-send-payments-body.png "Send body")

    This will initiate a send on the network that sends $$1000 from Member 0 to Member 1.
1.  Scroll down to Responses and save the `Response Body` JSON text.
    
    ![](assets/member-api-send-payments-response.png "Send response")
1.  Check the transaction status for the send claim transaction, and wait for it to be `confirmed`.

    ![](assets/member-api-send-status-confirmed.png "Send transaction status")
    
### Verify Member 1 Receives the Claims

1.  Open Member 1's API at [$MEMBER-1-API-BASE-URL$]($MEMBER-1-API-BASE-URL$)
1.  Verify Member 1's wallet has a balance of $$1000.

    ![](assets/member-api-send-final-result.png "Balance result")
    
You have now completed a Send!

## Redeem Workflow
Finally, you will redeem funds from Member 1's wallet to their bank account.

### Verify Starting Balance
1.  Open Member 1's API at [$MEMBER-1-API-BASE-URL$]($MEMBER-1-API-BASE-URL$) and authorize yourself with the following token:

    `$MEMBER-1-API-TOKEN$`

    ![](assets/member-api-ui.png "Member API User Interface")
1.  Verify that Member 1 has a non-zero balance in their wallet.

    ![](assets/member-api-send-final-result.png "Balance result")

    If Member 1 has no balance, then please go back and use the send workflow to transfer funds into Member 1's
    wallet before continuing.
1.  Verify the current bank account balance for Member 1's first account.
    Select `/accounts/{accountId}/balance` and click `Try it out`.
    
    ![](assets/member-api-redeem-bank-balance.png "Bank balance request")
1.  Enter `1` into the account id field and click `Execute`.

    ![](assets/member-api-redeem-bank-balance-parameters.png "Bank balance parameters")  
1.  Scroll down to responses and look at `currentBalanceInMinorUnit`.

    ![](assets/member-api-redeem-bank-balance-response.png "Bank balance response")
    
    By default, all bank accounts start with a $$10,000,000 balance. You will transfer money into this bank account,
    so the balance should increase following the completion of this workflow. 

### Redeem Claim

1.  Select `/transactions/claims/redeem` and  click `Try it out`.

    ![](assets/member-api-redeem-request.png "Redeem request")
1.  Paste the JSON formatted text below into the request body and click `Execute`.

    ```json
    {
        "accountId": 1,
        "amountInMinorUnit": 50000
    }
    ```

    ![](assets/member-api-redeem-request-body.png "Redeem request body")

    This action initiates a redemption for $$500. The Trustee software will produce a redemption transaction on the network and initiate a book transfer from the Trust account to Member 1's bank account.
1.  Scroll down to Responses and save the `Response Body` JSON text.

    ![](assets/member-api-redeem-request-response.png "Redeem request response")
1.  Wait for the transaction to be confirmed.

    ![](assets/member-api-redeem-status-confirmed.png "Transaction status")

### Verify Funds Transfer

1.  Get the bank balance for Member 1's bank account with id 1 again. 
    
    ![](assets/member-api-redeem-bank-balance-after.png "Bank balance response")

    The balance should be $$500 more than when you started. 

You have now completed the Redeem Workflow!
