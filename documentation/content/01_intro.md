# Xandbox

## Introduction

Xandbox is an entire Xand network running on a single machine. It is intended for the evaluation and demonstration
of the Xand network and its APIs. It runs software for Validators, Members, banks, and a Trustee, all connected
and operating on the same network. Each node's software runs in a Docker container orchestrated using Docker-Compose.

## Prerequisites

To use Xandbox, you will need:

- A Bash terminal
- [docker](https://docs.docker.com/get-docker/)
- [docker-compose](https://docs.docker.com/compose/install/)

## Launching the Network

Identify the folder where you unzipped Xandbox. For example, if you unzipped it to your downloads folder, it is likely in `~/Downloads/xandbox`.
Open a terminal window and run this command replacing `</path/to/xandbox>` with your Xandbox folder:

```bash
cd </path/to/xandbox>
cd network
docker-compose up -d
```

This operation will use `docker-compose` to launch a sandbox Xand network on your local machine.

## Post-Launch Configuration 

Sandbox Banks and Accounts must be configured for Members after launching the network.  
Use `$IMAGES.MEMBER-CONFIGURATION-IMAGE$` to configure Members' sandbox banks and accounts automatically.

Set `XANDBOX_DIR_ABS_PATH` to the *absolute path* to your Xandbox folder:
```bash
XANDBOX_DIR_ABS_PATH=</path/to/xandbox> # No trailing '/'
```

Then run:
```bash
docker run --rm \
  --network host \
  -v $${XANDBOX_DIR_ABS_PATH}/network:/mnt/xandbox/generated \
  $IMAGES.MEMBER-CONFIGURATION-IMAGE$
```

## Stopping the Network
Open a terminal window and run this command replacing  `</path/to/xandbox>` with your Xandbox folder:

```bash
cd </path/to/xandbox>
cd network
docker-compose down
```
