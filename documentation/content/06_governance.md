# Network Voting Walkthrough

Xand networks support a limited set of "governance" actions, which are taken through an agreement between
network participants. Such governance actions include the addition or removal of Members and
Validators, the designation of a Trustee, and the adjustment of the rate at which Validators are paid.

Changes are carried out via a Network Vote. Any Member or Validator is permitted to issue a proposal
to take some chosen action, and others can then vote to accept or reject that proposal. The required
threshold and voting restrictions vary by the type of proposal. 

The Network Voting Command-Line Interface (CLI) is a participant's primary means of interacting with
the Network Voting system on a Xand network. The CLI provides commands to query for current
proposals, issue your own, and vote on others' proposals.

This walkthrough will guide you through configuring the CLI to act as a particular participant in 
your Xandbox instance, issuing proposals, and voting on proposals.

## Acting as a Participant

### Background

All Network Voting actions are performed on behalf of a particular network participant. In a
production Xand network, each vote is at the discretion of the acting participant only.

In your local Xandbox network, you will vote on behalf of multiple Members and Validators,
experiencing the process from multiple perspectives.

The provided Xandbox instance comes with two Members (Member 0 and Member 1) and three Validators
(Validator 0, Validator 1 and Validator 2) on the network. It also includes an additional five
Members and five Validators who are not permissioned by default. 

> **Note:**
> As you begin to enact Network Voting proposals, the set of permissioned Members and Validators may
> change. Suppose your changes make it unnecessarily difficult to enact new proposals (e.g., due to a large
> number of assenting participants required for a vote to pass).  In that case, you can always shut down your Xandbox
> network, delete the directory, and follow the setup instructions to reset it.

### The "add member" Proposal

For this walkthrough, you will invite and accept Member 2 onto the network.

You can first confirm that Member 2 is not permissioned by attempting to transact with them.

Every Member has a unique identifier, called an "Address." Member 2's Address is `$MEMBER-2-ADDRESS$`.
Refer to [the "Send" walkthrough](#send-workflow) for the process of transacting with another
Member. If you attempt to send money from a wallet to Member 2, you will see the following response:

```json
{
  "XandClientError": {
    "message": "while issuing transfer",
      "source": {
        "NotFound": {
          "message": "\"InvalidMember\""
      }
    }
  }
}
```

Note the error "InvalidMember." Member 2 is not currently permissioned on the Xand network, and as
such, can't be included in transactions.

After completing this walkthrough, Member 2 will have joined the network, making them a valid counterparty.

### Start a New Session as Member 0

The Network Voting CLI is provided as a Docker image. See [this section](#accessing-transparents-container-registry)
if you have not already authorized your Docker client for Transparent's container registry.

Open a new, separate terminal window. This terminal will be configured to act as one chosen
participant. For this walkthrough, you will start with Member 0.

Navigate to the directory in which the Xandbox files are unzipped. For example, if you unzipped 
Xandbox into your Downloads folder, run:

```bash
cd ~/Downloads/xandbox
```

You will need to do this operation for all new terminals opened below.

**In this new terminal, run the following command:**

```bash
source ./nv_aliases.sh
act_as_member_0
```

You should now see a line of text of the form `member-0$$` appear:

> ```bash
> user@tpfs:~$$ source ./nv_aliases.sh
> user@tpfs:~$$ act_as_member_0
> member-0$$
> ```

> **Note:** Commands entered at the `member-0$$` prompt are run in an isolated environment
> where you won't have access to your other files or programs. You will want to explicitly exit this
> terminal once complete, which can be done by closing the terminal window or running the `exit`
> command.

To confirm that this session works, run:
```bash
network-voting-cli help
``` 

You should see a help message describing the usage of the Network Voting CLI. In addition, other related "help"
commands are listed at the bottom of this document, which can be referenced to discover new 
functionality of the CLI.

The `act_as_member_0` command you just ran is an _alias_ provided with Xandbox to make different
participant roles easily accessible. Internally, these aliases simply configure the CLI with the
participant's Address, Xand API URL, and authentication credentials. Thus, the Network Voting CLI is
configured to act as Member 0 in this terminal.

**Try it out to confirm that your connection settings are correct.**

```bash
network-voting-cli list
```

This command will list all active proposals. Since you haven't made any proposals, you should see
"No proposals found":

![](nv-cli-member-0-empty-list.png "Empty proposal list as Member 0")

You can have multiple terminal windows open at once, each configured to act as a different Member or
Validator. You will do so later in this guide for another Member and two Validators. For now, keep
your Member 0 session open.

## Make a Proposal

Now that you have a terminal with access to the Network Voting CLI acting as Member 0, you can issue
proposals.

You will vote Member 2 onto the network. In addition to the Address discussed above, this will also
require knowing Member 2's "encryption public key." You will provide both in the command shown
below. On a production Xand network, you would need to ask the prospective Member for this information.

**In the terminal window prepared in the last section, propose the addition of Member 2:**

```bash
network-voting-cli propose add-member $MEMBER-2-ADDRESS$ $MEMBER-2-ENCRYPTION-KEY$
```

After a short delay processing the transaction, you should see an output like the following:

![](nv-cli-new-member-proposal.png "New Member proposal output")

**View the active proposals:**

```bash
network-voting-cli list
```

You will now see the active proposal listed:

![](nv-cli-member-0-list.png "Proposal list after New Member proposal")

In the sample above, the proposal has been assigned proposal ID "0." Note that there is one
accepting Member vote -- Member 0, who proposed it -- and no other votes.

If this proposal remained open for the default expiration setting of 30 days, it
would be removed.

## Vote on the Proposal as Member 1

Now that a proposal exists, it's time to accrue enough "accept" votes that it is enacted.

For "add member" proposals, this requires greater than a 2/3rds supermajority of voting power, where
Members collectively represent 49% of the weight and Validators collectively represent 51%.

As with above, begin by opening a new terminal window in your Xandbox directory.

**In this new terminal, run the following command:**

```bash
source ./nv_aliases.sh
act_as_member_1
```

You should now see the prompt for Member 1, formatted as `member-1$$`, appear:

> ```
> user@tpfs:~$$ source ./nv_aliases.sh
> user@tpfs:~$$ act_as_member_1
> member-1$$
> ```

**List the active proposals from Member 1's perspective:**

```bash
network-voting-cli list
```

You should still see the active proposal listed:

![](nv-cli-member-1-list.png "Proposals List from Member 1 terminal")

Proposals are referred to by their IDs -- in this case, ID 0. You, as Member 1, want to vote to
"accept" the proposal with ID 0.

**Vote to accept proposal 0 with:**

```bash
network-voting-cli vote accept 0
```

Once the transaction has been finalized by the network, it will report a successful vote:

![](nv-cli-member-1-vote-yes.png "Member 1 successful vote")

**List the outstanding proposals to see both "accept" member votes:**

```bash
network-voting-cli list
```

![](nv-cli-member-1-list-post-vote.png "Proposals List after Member 1 vote")

## Vote on the Proposal as Validator 0 and Validator 1

Now, repeat the above process for two validators to reach the required supermajority.

For Validator 0:
```bash
exit
act_as_validator_0
network-voting-cli vote accept 0
```

For Validator 1:
```bash
exit
act_as_validator_1
network-voting-cli vote accept 0
```

After successfully voting your output will look like this: 

![](nv-cli-voting-with-validators.png "Validator voting for the proposal")

**Now that Validator 1 has submitted the final vote, check the proposal status again:**

```bash
network-voting-cli list
```

You will see that all four votes are cast, and the status is "Accepted":

![](nv-cli-vote-accepted.png "Vote accepted in proposals list")

## Confirm the Addition of Member 2

Congratulations, you have permissioned Member 2 onto the Xand network!

Refer to [the "Send" walkthrough](#send-workflow) to transact with them.

## Further Exploration

Xand supports other types of proposals not covered in this guide, as well as proposal rejections. If
you would like to exercise these aspects of the network, the following `network_voting_cli` commands
provide further guidance:

- `network-voting-cli help`
- `network-voting-cli propose help`
- `network-voting-cli vote help`

Your Xandbox `network` directory contains a file named `entities-metadata.yaml`, which describes all
addresses, URLs, authentication tokens, and encryption keys for the network you are running. This file is
where you should go to identify other participants' keys if you would like to add or remove any
participants other than Member 2.
